# FIXME:Project title

## Abstract, vision and goals

## Milestones

## Contributors

- jane doe, jane.doe@example.com, https://orcid.org/XXXX-XXXX-XXXX-XXXX, role:, responsibilities: , special skills

[Mention your contributors either by including a contributors list here, or by referring to another file that lists your contributors, see documentation for options and examples. It is good practice to set roles for each contributor at the start of the project and discuss author byline of a future paper early.]

## List of experiments

## Direct links (inside or outside this repository)

- todo list: [use issues](../../issues)
- data management plan: [01_project_management/05_data_management_plans](01_project_management/05_data_management_plans)
- Smartfigure tag: [YOURTAG](https://sdash.sourcedata.io/?q=YOURTAG) [FIXME:replace YOURTAG in the url and the text.]
- Main Communication channel: FIXME

## Other information

**Note:** This repository follows the Research repository template, v.2.3, see [further information offline](.doc/information.md) or [the online documentation page](https://gin-tonic.netlify.app/).

## Contribution

If you are new to this project, familiarise yourself with the vision, set your own role and responsibilities with the team, and read the data management plan. Make sure your contributions can be understood and reused by other members of the team.
